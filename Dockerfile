FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /fuzzycaller
WORKDIR /fuzzycaller
RUN pip install pipenv
ADD Pipfile /fuzzycaller/
ADD Pipfile.lock /fuzzycaller/
RUN pipenv install --system
ADD . /fuzzycaller/
ENTRYPOINT ["python", "manage.py"]
CMD ["runserver", "0.0.0.0:8000"]