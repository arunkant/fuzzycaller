# FuzzyCaller - Search contacts with ease

## features
* Import contacts from csv file, Export to csv file

* User authentication

* Search contacts

* Pagination

* Edit contacts

* Simple and clean design

* Docker support

## Installation

### with docker
1. build docker image

        $ cd ~/path/to/fuzzycaller
        $ docker build -t fuzzycaller .

2. run

        $ docker run -it -p8000:8000 fuzzycaller



### without docker
1. install pipenv

        $ pip install pipenv


2. install project

        $ cd ~/path/to/fuzzycaller
        $ pipenv install

3. run development server

        $ pipenv shell #use project's virtual environment
        $ python manage.py runserver

Go to http://localhost:8000/ 

## Accessing site
### use existing user
A user with username/password `rick/morty` has been created

### register new user
register new user at http://localhost:8000/accounts/register

## Notes
* Bootstrap and other assets are fetched from CDN. Kindly keep a active internet connection while testing

## Author
Arun Kant Sharma (mail@arunkant.com)