from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.paginator import Paginator
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django import forms

import logging, csv
from io import StringIO
from .models import Contact

from .forms import UploadFileForm, UserRegistrationForm, ContactForm
# Create your views here.

logger = logging.getLogger(__name__)

@login_required
def index(request):
    contact_list = Contact.objects.all().order_by('id')
    paginator = Paginator(contact_list, 15)  

    page = request.GET.get('page')
    contacts = paginator.get_page(page)
    return render(request, 'app/index.html', {'contacts': contacts})

@login_required
def upload(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            old_count = Contact.objects.count()
            Contact.objects.all().delete()
            reader = csv.reader(StringIO(request.FILES['file'].read().decode('utf-8')))
            next(reader) # remove the headers
            new_count = 0
            for row in reader:
                contact = Contact(name=row[1], number=row[2])
                contact.save()
                new_count += 1
            return render(request, 'app/csv_uploaded.html', {
                'old_count': old_count,
                'new_count': new_count
            })
    else:
        form = UploadFileForm()
    return render(request, 'app/upload.html', {'form': form})

@login_required
def export(request):
    inMemoryFile = StringIO()
    writer = csv.writer(inMemoryFile, quoting=csv.QUOTE_ALL)
    writer.writerow(["", "name", "number"])
    for contact in Contact.objects.all():
        writer.writerow([contact.id, contact.name, contact.number])
    output = inMemoryFile.getvalue()

    response = HttpResponse(output, content_type="application/csv")
    response['Content-Disposition'] = "attachment; filename=phone-numbers.csv"
    response['Content-Length'] = len(output.encode('utf-8'))
    return response


@login_required
def search(request):
    query = request.GET.get('q', '')
    if not query:
        return redirect('index')
    contact_list = Contact.objects.filter(name__icontains=query).order_by('id')
    paginator = Paginator(contact_list, 15)

    page = request.GET.get('page')
    contacts = paginator.get_page(page)
    return render(request, 'app/search.html', {'contacts': contacts, 'query': query })

def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            username = userObj['username']
            email =  userObj['email']
            password =  userObj['password']
            if not (User.objects.filter(username=username).exists() or User.objects.filter(email=email).exists()):
                User.objects.create_user(username, email, password)
                user = authenticate(username = username, password = password)
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                raise forms.ValidationError('Looks like a username with that email or password already exists')
    else:
        form = UserRegistrationForm()
    return render(request, 'app/register.html', {'form' : form})

@login_required
def update(request, contact_id):
    instance = get_object_or_404(Contact, pk=contact_id)
    if request.method == 'POST':
        form = ContactForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return render(request, 'app/update.html', {'form': form, 'message': 'Successfully saved'})
        else:
            return render(request, 'app/update.html', {'form': form, 'message': 'Error: Could not save'})
    else:
        form = ContactForm(instance=instance)
    return render(request, 'app/update.html', {'form': form})